#include "Vector.h"
#include <iostream>


Vector::Vector()//обнуляем все
{
	for(int i =0;i<3;i++)
	{
		First_V[i] = 0;
		Second_V[i] = 0;
		Middle_Dot[i] = 0;
	}
	LenghtAB = 0;
	std::cout << "Const 1 created\n";
}

Vector::Vector(double x, double y, double z)//задаем значения с помощью параметров
{
	First_V[0] = x;
	First_V[1] = y;
	First_V[2] = z;
	std::cout << "Const z parametrami created\n";
}

Vector::Vector(double x, double y)
{
	Second_V[0] = x;
	Second_V[1] = y;
	Second_V[2] = 0;
	std::cout << "Const with def. param \n";
}

Vector::Vector(const Vector& src)
{
	std::cout << "Copy Const\n";
	for (int i = 0; i < 3; i++)
	{
		First_V[i] = src.First_V[i];
		Second_V[i] = src.Second_V[i];
	}
}

Vector::Vector(double ax, double ay, double az, double bx, double by, double bz)// задаем два вектора
{
	First_V[0] = ax;
	First_V[1] = ay;
	First_V[2] = az;
	Second_V[0] = bx;
	Second_V[1] = by;
	Second_V[2] = bz;
	Middle_Dot[0] = 0;
	Middle_Dot[1] = 0;
	Middle_Dot[2] = 0;
	std::cout << "Vectors was Created\n";
}


Vector::~Vector()
{
	std::cout << "\nDestructor\n";
}


void Vector::setFV(double ax, int i)
{
	First_V[i] = ax;
}


void Vector::setSV(double bx,int i)
{
	Second_V[i] = bx;
}

void Vector::getFV()
{
	for (int i = 0; i < 3; i++) std::cout << "First V[" << i << "]= " << First_V[i]<< "\n";
}

void Vector::getSV()
{
	for (int i = 0; i < 3; i++) std::cout <<"Second V["<<i<<"]= " <<Second_V[i]<<"\n";
}

void Vector::findMD()
{
	for (int i = 0; i < 3; i++)
	{
		Middle_Dot[i] = (First_V[i]) / 2;
	}
}

void Vector::getMD()
{
	findMD();
	for (int i = 0; i < 3; i++)
	{
		std::cout << "Middle D[" << i << "]= " << Middle_Dot[i]<< "\n";
	}

}


void Vector::findLength()
{
	for(int i=0;i<3;i++)
	LenghtAB += sqrt(pow((First_V[i]),2));

}

double Vector::getLength()
{
	findLength();
	return LenghtAB;
}

void Vector::findSumma()
{
	for (int i = 0; i < 3; i++)
	{
		SummaAB[i] = (First_V[i] + Second_V[i]);
	}
}

void Vector::getSumma()
{
	findSumma();
	for (int i = 0; i < 3; i++)
	{
		std::cout << "Summa[" << i << "]= " << SummaAB[i] << "\n";
	}
}

void Vector::findRaznost()
{
	for (int i = 0; i < 3; i++)
	{
		Raznost[i] = (First_V[i] - Second_V[i]);
	}
}

void Vector::getRaznost()//ищем вектор А - вектор Б
{
	findRaznost();
	for (int i = 0; i < 3; i++)
	{
		std::cout << "Raznost[" << i << "]= " << Raznost[i] << "\n";
	}
}

void Vector::findKnaA(double k)
{
	First_V[0] = k* First_V[0];
	First_V[1] = k* First_V[1];
	First_V[2] = k* First_V[2];
}

void Vector::findKnaB(double k)
{
	Second_V[0] = k* Second_V[0];
	Second_V[1] = k* Second_V[1];
	Second_V[2] = k* Second_V[2];
}

void Vector::findScalarC()//ищем скалярное произведение
{
	ScalarCoordinaty = (First_V[0] * Second_V[0]) + (First_V[1] * Second_V[1]) + (First_V[2] * Second_V[2]);
}

double Vector::getScalarC()
{
	findScalarC();
	return ScalarCoordinaty;
}

