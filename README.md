# Laba4

* Змоделювати клас для заданого поняття (згідно варіанту).
    - визначити атрибути класу та їх типи даних;
    - визначити методи класу
* До методів класу додати: 
    - конструктори за замовчуванням, з параметрами, зі списком ініціалізації;
    - деструктор.
* Описати клас в UML-нотації.
* Написати програму, в якій користувач матиме можливість проводити маніпуляції зі створеним класом. Організувати роботу з масивом екземплярів класу.
