#ifndef VECTOR_H
#define VECTOR_H
#include <cmath>

class Vector
{
	double First_V[3];//первый вектор с координатами (x,y,z)
	double Second_V[3];//второй вектор с координатами (x,y,z)
	double Middle_Dot[3];
	double LenghtAB=0;//длина вектора
	double SummaAB[3];//сумма векторов
	double Raznost[3];//разность векторов
	double ScalarCoordinaty=0;//скаярное произведение через координаты
	void findLength();
	void findMD();
	void findSumma();
	void findRaznost();
	void findScalarC();
	void findKnaA(double k);//число К умножить на вектор
	void findKnaB(double k);
public:
	Vector();//конструктор по умолчанию
	Vector(double x, double y, double z);//констр с параметрами
	Vector(double x, double y=0);//констр с параметром по умолчанию
	Vector(const Vector& src);//констр копирования
	Vector(double ax, double ay, double az, double bx, double by, double bz);//задаем два вектора
	~Vector();//деструктор
	void setFV(double ax,int i);
	void getFV();
	void setSV(double bx,int i);
	void getSV();
	void getMD();
	double getLength();
	void getSumma();
	void getRaznost();
	double getScalarC();
};
#endif
